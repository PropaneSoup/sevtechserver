FROM java:8

WORKDIR /minecraft

COPY files/eula.txt /minecraft/eula.txt

RUN wget https://media.forgecdn.net/files/3047/442/SevTech-Ages_Server_3.1.7.zip \
    && unzip SevTech-Ages_Server_3.1.7.zip \
    && rm SevTech-Ages_Server_3.1.7.zip
RUN chmod u+x Install.sh ServerStart.sh
RUN sed -i '2i /bin/sh /minecraft/CheckEula.sh' /minecraft/ServerStart.sh
RUN /minecraft/Install.sh

ENV EULA="true"
